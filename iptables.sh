#!/bin/bash

export IPT="iptables"

export WAN=enp0s3
export WAN_IP=10.0.2.1

#CLEAN
$IPT -F
$IPT -F -t nat
$IPT -F -t mangle
$IPT -X
$IPT -t nat -X
$IPT -t mangle -X

#### MANGLE ####
#SPECIFY SIZE
$IPT -t mangle -A INPUT -p all  --match length --length 10001: -j DROP

### CHECK DOUBLE SSH
$IPT -t mangle -A INPUT -p tcp --dport 42 -m connlimit --connlimit-above 1 -j DROP
#$IPT -t mangle -A INPUT -p tcp --dport 42 -s 127.0.0.1 -j MARK --set-mark 666
#$IPT -t mangle -A PREROUTING -p tcp --dport 42 -m mac ! --mac-source 52:54:00:12:35:02 -j MARK --set-mark 666
#$IPT -t mangle -A PREROUTING -p tcp --dport 42 -m mac ! --mac-source 78:7b:8a:d4:c1:7c -j MARK --set-mark 666
#$IPT -t mangle -I INPUT 1 -p all -j ACCEPT

######BRUTFORCE_CHECK###########
$IPT -N ssh_bf
$IPT -A ssh_bf -p tcp --dport 42 -m conntrack --ctstate NEW -m recent --update --seconds 60 --hitcount 3 -j DROP
#$IPT -A ssh_bf -j DROP
$IPT -A ssh_bf -m recent --set -j ACCEPT
#$IPT -A ssh_bf -p tcp ! --dport 42 -j ACCEPT
$IPT -I ssh_bf 1 -p tcp ! --dport 42 -j ACCEPT
#######

###### DDOS PROTECTION #######
$IPT -N ddos
$IPT -A ddos -p all -m conntrack --ctstate NEW -m limit --limit 20/sec --limit-burst 20 -j ACCEPT
######

#DEFAULT
$IPT -P INPUT DROP
$IPT -P OUTPUT DROP
$IPT -P FORWARD DROP

#LOCAL
$IPT -A INPUT -i lo -j ACCEPT
$IPT -A OUTPUT -o lo -j ACCEPT

#PING ALLOW
$IPT -A INPUT -p icmp --icmp-type echo-reply -j ddos
$IPT -A INPUT -p icmp --icmp-type destination-unreachable -j ddos
$IPT -A INPUT -p icmp --icmp-type time-exceeded -j ddos
$IPT -A INPUT -p icmp --icmp-type echo-request -j ddos

#ALLOW OUTPUT
$IPT -A OUTPUT -o $WAN -j ACCEPT

#DIFFERENT MTU
#$IPT -I FORWARD -p tcp --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu

#7 Port_Scan Check
$IPT -I INPUT 1 -m recent --set 
#$IPT -I INPUT 1 -m conntrack --ctstate NEW -p udp -m multiport --dport 53,123 -j ddos
$IPT -I INPUT 1 -m conntrack --ctstate NEW -p tcp -m multiport --dport 42,80,443 -j ddos
$IPT -I INPUT 1 -p tcp -m recent --rcheck --seconds 60 --hitcount 2 -j REJECT
$IPT -I INPUT 1 -p tcp -m recent --rcheck --seconds 3600 --hitcount 10 -j REJECT
#$IPT -I INPUT 1 -p udp -m recent --rcheck --seconds 60 --hitcount 2 -j REJECT
#$IPT -I INPUT 1 -p udp -m recent --rcheck --seconds 3600 --hitcount 10 -j REJECT

#$IPT -I INPUT 1 -p all -j ACCEPT

#6 Brutforce check
$IPT -I INPUT 1 -m conntrack --ctstate NEW -p tcp --dport 42 -j ssh_bf

#5 ALLOW ESTABLISHED/RELATED CONNECTIONS
$IPT -I INPUT 1 -p all -m state --state ESTABLISHED,RELATED -j ACCEPT
$IPT -I OUTPUT 1 -p all -m state --state ESTABLISHED,RELATED -j ACCEPT
$IPT -I FORWARD 1 -p all -m state --state ESTABLISHED,RELATED -j ACCEPT

#4 SPUFFING_PROTECTION
$IPT -A INPUT -p tcp -m addrtype --src-type LOCAL ! -i lo -j REJECT --reject-with tcp-reset
$IPT -A INPUT -p udp -m addrtype --src-type LOCAL ! -i lo -j REJECT
$IPT -A INPUT -m conntrack --ctstate NEW,INVALID -p tcp --tcp-flags SYN,ACK SYN,ACK -j REJECT --reject-with tcp-reset

#3 DROP INVALID PACKAGES
$IPT -I INPUT 1 -m state --state INVALID -j DROP
$IPT -I FORWARD 1 -m state --state INVALID -j DROP

#2 DECLINE PACKAGES FROM NOT ESTABLISHED CONTACTS
$IPT -I INPUT 1 -p tcp ! --syn -m state --state NEW -j DROP
$IPT -I OUTPUT 1 -p tcp ! --syn -m state --state NEW -j DROP

#1 REJECT 666
#$IPT -I INPUT 1 -p tcp -m mark --mark 666/666 -j REJECT --reject-with tcp-reset

#$IPT -I INPUT 1 -p all -j ACCEPT

/sbin/iptables-save > /etc/iptables_rules
